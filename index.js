const express = require('express');
const cors = require('cors');

const jwt = require('jsonwebtoken');
const { MongoClient, ServerApiVersion, ObjectId } = require('mongodb');
require('dotenv').config();
const port = process.env.PORT || 5000;


const app = express();

//middleware
app.use(cors());
app.use(express.json());

//Verify User JWT Authentication
const verifyJwt = (req, res, next) => {
    const authorization = req.headers.authorization;

    // if the bearer not found
    if (!authorization) {
        res.status(401).send({ message: 'Unauthorized Access' });
    } else {
        // get access token
        const token = authorization.split(' ')[1];
        jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, decoded) => {
            // access token decoded failed
            if (err) {
                return res.status(403).send({ message: 'Forbidden Access' });
            } else {
                // access token decoded successfully
                req.decoded = decoded;
                next();
            }
        });
    }
};

//Connect monoDB Database
const uri = `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASS}@cluster0.atbzeoy.mongodb.net/?retryWrites=true&w=majority`;
const client = new MongoClient(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    serverApi: ServerApiVersion.v1
});

const run = async () => {
    await client.connect();
    try {
        const itemsCollection = client.db('shipidoInventory').collection('items');

        const brandCollection = client.db('shipidoInventory').collection('brand');

        const categoryCollection = client.db('shipidoInventory').collection('category');

        const supplierCollection = client.db('shipidoInventory').collection('supplier');

        const employeeCollection = client.db('shipidoInventory').collection('employees');

        const clientCollection = client.db('shipidoInventory').collection('clients');

        const deliveryCollection = client.db('shipidoInventory').collection('delivery');

        const faqCollection = client.db('shipidoInventory').collection('faqs');

        const supportCollection = client.db('shipidoInventory').collection('support');

        // get total sales
        const getTotalSales = async () => {
            const cursor = deliveryCollection.aggregate([
                {
                    $group: {
                        _id: null,
                        totalSales: { $sum: { $multiply: ["$product.sellingPrice", "$deliveryAmount"] } }
                    }
                }]);

            const result = await cursor.toArray();

            return result[0].totalSales;
        }

        // get total purchase
        const getTotalPurchase = async () => {
            const cursor = deliveryCollection.aggregate([
                {
                    $group: {
                        _id: null,
                        totalPurchase: { $sum: { $multiply: ["$product.buyingPrice", "$deliveryAmount"] } }
                    }
                }]);

            const result = await cursor.toArray();

            return result[0].totalPurchase;
        }

        // get total stock
        const getTotalStock = async () => {
            const cursor = itemsCollection.aggregate([
                { $group: { _id: null, totalStock: { $sum: "$stock" } } }
            ]);

            const result = await cursor.toArray();

            return result[0].totalStock;
        }

        // get Total Suppliers
        const getTotalSuppliers = async () => {
            const query = {}
            return await supplierCollection.countDocuments(query);
        }

        // get Total Clients
        const getTotalClients = async () => {
            const query = {}
            return await clientCollection.countDocuments(query);
        }

        // get Total # of Sales
        const getNumberOfSales = async () => {
            const query = {}
            return await deliveryCollection.countDocuments(query);
        }

        // get Total Categories
        const getTotalCategories = async () => {
            const query = {}
            return await categoryCollection.countDocuments(query);
        }

        // Authentication
        app.post('/login', async (req, res) => {
            const user = req.body;
            console.log({ user })
            const accessToken = jwt.sign(
                user,
                process.env.ACCESS_TOKEN_SECRET,
                {
                    expiresIn: '2d',
                }
            );
            res.send({ accessToken });
        });

        //API for All Products/Items
        app.get('/items', async (req, res) => {
            let page = parseInt(req.query.page);
            const filter = parseInt(req.query.filter);
            const query = {};
            const cursor = itemsCollection.find(query);

            let result;
            if (page || filter) {
                // Solves the items skips
                if (page > 0) {
                    page -= 1;
                }
                result = await cursor.skip(page * filter).limit(filter).toArray();
            } else {
                result = await cursor.toArray();
            }
            res.send(result);
        });

        // Items Pages
        app.get('/items-pages', async (req, res) => {
            const count = await itemsCollection.estimatedDocumentCount();
            res.send({ count });
        });

        //API to get low stock Inventory
        app.get('/low-stock-products', async (req, res) => {
            const query = ({ stock: { $lt: 15 } });
            const cursor = itemsCollection.find(query).sort({ stock: 1 });
            const lowStock = await cursor.toArray();
            res.send(lowStock);
        });


        //    API for Insert New Item to Database
        app.post('/items', async (req, res) => {
            const item = req.body;
            const result = await itemsCollection.insertOne(item);
            res.send(result);
        });


        //API for get Brand Names
        app.get('/brand', async (req, res) => {
            const query = {};
            const cursor = brandCollection.find(query).sort({ name: 1 });
            const brands = await cursor.toArray();
            res.send(brands);
        });

        //API for get Category List
        app.get('/category', async (req, res) => {
            const query = {};
            const cursor = categoryCollection.find(query).sort({ name: 1 });
            const categories = await cursor.toArray();
            res.send(categories);
        });

        //API for get Suppliers Names
        app.get('/supplier', async (req, res) => {
            const query = {};
            const cursor = supplierCollection.find(query).sort({ name: 1 });
            const suppliers = await cursor.toArray();
            res.send(suppliers);
        });

        //API to get Employee Data
        app.get('/employees', async (req, res) => {
            const query = {};
            const cursor = employeeCollection.find(query);
            const employees = await cursor.toArray();
            res.send(employees);
        });

        //API to get all Suppliers Data
        app.get('/suppliers', async (req, res) => {
            const query = {};
            const cursor = supplierCollection.find(query);
            const employees = await cursor.toArray();
            res.send(employees);
        });

        //API to get all Clients Data
        app.get('/clients', async (req, res) => {
            const query = {};
            const cursor = clientCollection.find(query);
            const clients = await cursor.toArray();
            res.send(clients);
        });

        //API to get all Delivery Data
        app.get('/deliveries', async (req, res) => {
            const query = {};
            const cursor = deliveryCollection.find(query);
            const deliveries = await cursor.toArray();
            res.send(deliveries);
        });

        //API to get all Faqs
        app.get('/faqs', async (req, res) => {
            const query = {};
            const cursor = faqCollection.find(query);
            const faqs = await cursor.toArray();
            res.send(faqs);
        });

        // API to get dashboard data
        app.get('/dashboard-data', async (req, res) => {
            const dashboard = {
                totalStock: 0,
                totalCategory: 0,
                totalSupplier: 0,
                totalClients: 0,
                countSales: 0,
                totalPurchase: 0,
                totalSales: 0,
                totalProfit: 0,
            }
            // get total stock
            dashboard.totalStock = await getTotalStock();
            // get total suppliers
            dashboard.totalSupplier = await getTotalSuppliers();
            // get total category
            dashboard.totalCategory = await getTotalCategories();
            // get total clients
            dashboard.totalClients = await getTotalClients();
            // get total # of Sales
            dashboard.countSales = await getNumberOfSales();
            // get total sales
            dashboard.totalSales = await getTotalSales();
            // get total purchase
            dashboard.totalPurchase = await getTotalPurchase();
            // get total profit
            dashboard.totalProfit = dashboard.totalSales - dashboard.totalPurchase;

            res.send({ dashboard });
        });

        //API to get single item by ID
        app.get('/item/:id', async (req, res) => {
            const id = req.params.id;
            const query = { _id: ObjectId(id) };
            const item = await itemsCollection.findOne(query);
            res.send(item);
        });

        //API to get single Employee by ID
        app.get('/employee/:id', async (req, res) => {
            const id = req.params.id;
            const query = { _id: ObjectId(id) };
            const item = await employeeCollection.findOne(query);
            res.send(item);
        });

        //API to get single Supplier by ID
        app.get('/supplier/:id', async (req, res) => {
            const id = req.params.id;
            const query = { _id: ObjectId(id) };
            const item = await supplierCollection.findOne(query);
            res.send(item);
        });

        //API to get single delivery details
        app.get('/delivery-details/:id', async (req, res) => {
            const id = req.params.id;
            const query = { _id: ObjectId(id) };
            const cursor = deliveryCollection.find(query);
            const deliveryDetails = await cursor.toArray();
            res.send(deliveryDetails);
        });

        //API to get single Client details
        app.get('/client/:id', async (req, res) => {
            const id = req.params.id;
            const query = { _id: ObjectId(id) };
            const cursor = clientCollection.find(query);
            const clientDetails = await cursor.toArray();
            res.send(clientDetails[0]);
        });

        // API for Insert New Employee to Database
        app.post('/employee', async (req, res) => {
            const employee = req.body;
            const result = await employeeCollection.insertOne(employee);
            res.send(result);
        });

        // API for Insert New Supplier to Database
        app.post('/supplier', async (req, res) => {
            const supplier = req.body;
            const result = await supplierCollection.insertOne(supplier);
            res.send(result);
        });

        // API to Insert Support Message
        app.post('/support', async (req, res) => {
            const support = req.body;
            const result = await supportCollection.insertOne(support);
            res.send(result);
        });

        // API for Insert New Client to Database
        app.post('/client', async (req, res) => {
            const client = req.body;
            const result = await clientCollection.insertOne(client);
            res.send(result);
        });

        //API to delete single item
        app.delete('/item/:id', async (req, res) => {
            const id = req.params.id;
            const query = { _id: ObjectId(id) };
            const result = await itemsCollection.deleteOne(query);
            res.send(result);
        });

        //API to delete single Employee
        app.delete('/employee/:id', async (req, res) => {
            const id = req.params.id;
            const query = { _id: ObjectId(id) };
            const result = await employeeCollection.deleteOne(query);
            res.send(result);
        });

        //API to delete single Supplier
        app.delete('/supplier/:id', async (req, res) => {
            const id = req.params.id;
            const query = { _id: ObjectId(id) };
            const result = await supplierCollection.deleteOne(query);
            res.send(result);
        });

        //API to delete single Client
        app.delete('/client/:id', async (req, res) => {
            // console.log("id", id)
            const id = req.params.id;
            const query = { _id: ObjectId(id) };
            const result = await clientCollection.deleteOne(query);
            res.send(result);
        });


        //API to update item's stock
        app.put('/stock-update/:id', async (req, res) => {
            const itemId = req.params.id;
            console.log({ itemId })
            const data = req.body;
            console.log({ data })

            // update item's stock
            const filter = { _id: ObjectId(itemId) };
            const options = { upsert: true };
            const updateDoc = {
                $set: {
                    stock: data?.stock
                }
            };

            const updatedProductStockResult = await itemsCollection?.updateOne(
                filter,
                updateDoc,
                options
            );
            res.send(updatedProductStockResult);
        });

        //API to delivery item and also update item's stock amount
        app.put('/product-delivery/:id', async (req, res) => {
            const productId = req.params.id;
            console.log({ productId })
            const data = req.body;
            console.log({ data })

            // update product stock
            const filter = { _id: ObjectId(productId) };
            const options = { upsert: true };
            const updateDoc = {
                $set: {
                    stock: data?.stock
                }
            };

            const updatedProductStockResult = await itemsCollection?.updateOne(
                filter,
                updateDoc,
                options
            );

            console.log({ updatedProductStockResult })
            // get product doc
            const productDocument = await itemsCollection.findOne({ _id: ObjectId(productId) });
            const newDeliveryDocument = {
                product: productDocument,
                client: req.body.client,
                deliveryAmount: req.body.deliveryAmount,
                deliveryDate: new Date()
            }

            console.log({ newDeliveryDocument })
            // create new delivery document
            const result = await deliveryCollection.insertOne(newDeliveryDocument);
            console.log({ result })
            // if successfully delivery doc created then send response
            res.send(updatedProductStockResult);
        });


        //API to Update Employee Information
        app.put('/employee/:id', async (req, res) => {
            const id = req.params.id;
            const updatedEmployee = req.body;
            console.log({ updatedEmployee })
            const filter = { _id: ObjectId(id) };
            const options = { upsert: true };
            updatedEmployee._id = ObjectId(id);
            const updateDoc = {
                $set: {
                    first_name: updatedEmployee?.first_name,
                    last_name: updatedEmployee?.last_name,
                    position: updatedEmployee?.position,
                    email: updatedEmployee?.email,
                    image: updatedEmployee?.image,
                    address: updatedEmployee?.address,
                    mobile_number: updatedEmployee?.mobile_number,
                    marital_status: updatedEmployee?.marital_status,
                    emergency_contact: updatedEmployee?.emergency_contact,

                }
            };

            const result = await employeeCollection?.updateOne(
                filter,
                updateDoc,
                options
            );
            res.send(result);
        });

        //API to Update Supplier Information
        app.put('/supplier/:id', async (req, res) => {
            const id = req.params.id;
            const updatedSupplier = req.body;
            const filter = { _id: ObjectId(id) };
            const options = { upsert: true };
            updatedSupplier._id = ObjectId(id);
            const updateDoc = {
                $set: {
                    name: updatedSupplier?.name,
                    email: updatedSupplier?.email,
                    address: updatedSupplier?.address,
                    contact: updatedSupplier?.contact

                }
            };
            const result = await supplierCollection?.updateOne(
                filter,
                updateDoc,
                options
            );
            res.send(result);
        });


        //API to Update Product Details
        app.put('/product/:id', async (req, res) => {
            const id = req.params.id;
            const updatedProduct = req.body;
            const filter = { _id: ObjectId(id) };
            const options = { upsert: true };
            updatedProduct._id = ObjectId(id);
            const updateDoc = {
                $set: {
                    name: updatedProduct?.name,
                    brand: updatedProduct?.brand,
                    description: updatedProduct?.description,
                    category: updatedProduct?.category,
                    buyingPrice: updatedProduct?.buyingPrice,
                    sellingPrice: updatedProduct?.sellingPrice,
                    stock: updatedProduct?.stock,
                    supplier: updatedProduct?.supplier,
                    image: updatedProduct?.image

                }
            };
            const result = await itemsCollection?.updateOne(
                filter,
                updateDoc,
                options
            );
            res.send(result);
        });

        //API to Update Client Information
        app.put('/client/:id', async (req, res) => {
            const id = req.params.id;
            const updatedClient = req.body;
            console.log({ updatedClient })
            const filter = { _id: ObjectId(id) };
            const options = { upsert: true };
            updatedClient._id = ObjectId(id);
            const updateDoc = {
                $set: {
                    clientName: updatedClient?.clientName,
                    clientEmail: updatedClient?.clientEmail,
                    clientCompanyName: updatedClient?.clientCompanyName,
                    clientContactNumber: updatedClient?.clientContactNumber,
                    clientAddress: updatedClient?.clientAddress,
                }
            };

            const result = await clientCollection?.updateOne(
                filter,
                updateDoc,
                options
            );
            res.send(result);
        });


    } finally {

    }
};

run().catch(console.dir);


app.get('/', (req, res) => {
    res.send('Running Shipido Inventory Management');
});

app.listen(port, () => {
    console.log("Listening to Port: ", port);
});

module.exports = app;